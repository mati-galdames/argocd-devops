#!/bin/bash

while true; do
  running_pods=$(kubectl get pods -n argocd | grep -c Running)
  if [ "$running_pods" -eq 7 ]; then
    break
  fi
  echo "The number of running pods is not 7. Current count: $running_pods. Waiting..."
  sleep 5
done
echo "All 7 pods are running."

kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'