#!/bin/bash

mkdir -p ${HOME}/app

touch ${HOME}/app/application.yml
cat > ${HOME}/app/application.yml << EOF
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: br-ms-sd00026-oi00014-crudtemplate 
  namespace: argocd
spec:
  project: default

  source:
    repoURL: https://gitlab.com/mati-galdames/br-ms-sd00026-oi00014-crudtemplate.git
    targetRevision: HEAD
    path: br-ms-sd00026-oi00014-crudtemplate
  destination: 
    server: https://kubernetes.default.svc
    namespace: kube-system
EOF

touch ${HOME}/app/application2.yml
cat > ${HOME}/app/application2.yml << EOF
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: guestbook
  namespace: argocd
spec:
  project: default

  source:
    repoURL: https://github.com/argoproj/argocd-example-apps.git
    targetRevision: HEAD
    path: guestbook
  destination: 
    server: https://kubernetes.default.svc
    namespace: kube-system
EOF

touch ${HOME}/app/application3.yml
cat > ${HOME}/app/application3.yml << EOF
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: guestbook2
  namespace: argocd
spec:
  project: default

  source:
    repoURL: https://github.com/argoproj/argocd-example-apps.git
    targetRevision: HEAD
    path: helm-guestbook
  destination: 
    server: https://kubernetes.default.svc
    namespace: kube-system
EOF

touch ${HOME}/app/application4.yml
cat > ${HOME}/app/application4.yml << EOF
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: my-app-node
  namespace: argocd
spec:
  project: default

  source:
    repoURL: https://gitlab.com/mati-galdames/demo-devops.git
    targetRevision: HEAD
    path: .
  destination: 
    server: https://kubernetes.default.svc
    namespace: kube-system
EOF